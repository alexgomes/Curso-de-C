#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {

	// imprime cabecalho do nosso jogo
	printf("******************************************\n");
	printf("* Bem vindo ao nosso jogo de adivinhacao *\n");
	printf("******************************************\n");

	int segundos = time(0);
	srand (segundos);
	int numerogrande = rand ();
	
	int numerosecreto = numerogrande % 100;
	int chute;
	int tentativas;
	double pontos = 1000;
	
	int acertou = 0;
	int dificuldade;
	int i;
	
	int nivel;
	printf("Qual o nivel de dificuldade que voce deseja?\n");
	printf("Digite (1) Dificil - (2) Medio - (3) Facil: \n");
	scanf("%d",&dificuldade);
	
	if (dificuldade == 1){
		tentativas = 6;
	} else if (dificuldade == 2){
		tentativas = 12;
	} else if (dificuldade == 3){
		tentativas = 20;
	}
	

	for (i = 1; i <= tentativas; i++){
	

		printf("\nTentativa %d\n", i);
		printf("Qual eh o seu chute? ");

		scanf("%d", &chute);
		printf("Seu chute foi %d\n", chute);

		if(chute < 0) {
			printf("Voce nao pode chutar numeros negativos!\n");
			continue;
		}

		int acertou = (chute == numerosecreto);
		int maior = chute > numerosecreto;

		if(acertou) {
			printf("Parabens! Voce acertou!\n");
			printf("Jogue de novo, voce eh um bom jogador!\n");

			break;
		}

		else if(maior) {
			printf("Seu chute foi maior que o numero secreto\n");
		} 

		else {
			printf("Seu chute foi menor que o numero secreto\n");
		}
		
		double pontosperdidos = abs(chute - numerosecreto) / (double)2;
		pontos = pontos - pontosperdidos;
	}

	printf("Fim de jogo!\n");
	
	if (chute == numerosecreto){
		printf("Voce ganhou!\n");
		printf("Voce acertou em %d tentativas!\n", i
		);
		printf("Seu total de pontos: %.1f\n\n", pontos);
	} else {
		printf("Voce perdeu, nao desista. Tente novamente!\n");
	}
}







